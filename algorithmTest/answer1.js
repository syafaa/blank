// Case :
// - create a function that accept single parameter. the parameter will be an array of integer
// - your function should be able to count the unique value inside the array
// - cannot use array build-in function
// - examples:
//   - `countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]))` => 7
//   - `countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]))` => 4
//   - `countUniqueValues([]))` => 0

function uniqueValue(value, index, array) {
  return array.indexOf(value) === index;
}
const values = [1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13];

const unique = values.filter(uniqueValue);
console.log(unique.length);
